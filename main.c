#include <errno.h>
#include <ncurses.h>
#include <stdio.h>
#include <stdlib.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>

#define IN_SEC 3000000
#define OUT_SEC 4000000

int main()
{
	int x, y;
	unsigned num_cells;
	unsigned sleep_time;

	// Init ncurses
	initscr();
	// Disable buffering, get input char at a time
	cbreak();
	// Dont print input characters on screen
	noecho();

	// Seed the pRNG
	unsigned seed = (unsigned)time(NULL);

	// Make getch() non-blocking
	nodelay(stdscr, true);
	while (getch() == ERR) {
		srand(seed);
		num_cells = COLS * LINES;
		sleep_time = IN_SEC / num_cells;
		for (unsigned i = 0; i < num_cells; i++) { // Breathe in
			x = rand() % COLS;
			y = rand() % LINES;
			move(y, x);
			addch('0');
			refresh();
			usleep(sleep_time);
		}
		srand(seed);
		num_cells = COLS * LINES;
		sleep_time = OUT_SEC / num_cells;
		for (unsigned i = 0; i < num_cells; i++) { // Breathe out
			x = rand() % COLS;
			y = rand() % LINES;
			move(y, x);
			addch(' ');
			refresh();
			usleep(sleep_time);
		}
	}
	endwin();

	return 0;
}
